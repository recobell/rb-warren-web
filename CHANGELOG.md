## [1.0.4](http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/js/rb-warren-jsonp-1.0.4.jq.js) (2016-12-28)

### Bug Fixes
* change loadScript function name to loadScript2 to avoid conflict with other library
* use count value to limit the number of items to be displayed

## [1.0.3](http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/js/rb-warren-jsonp-1.0.3.jq.js) (2016-12-28)

### Features
* remove *stock* value checking for general purpose

## [1.0.2](http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/js/rb-warren-jsonp-1.0.2.jq.js) (2016-11-08)

### Features
* add *baseUrl* parameter for *itemImage* value loading issue
* check *stock* value for [nubizio](http://www.nubizio.co.kr/)

## [1.0.1](http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/js/rb-warren-jsonp-1.0.1.jq.js) (2016-08-07)


### Bug Fixes
* exeception handling for *extraImage* value
* hide recmd content on error

### Features
* add *st* query parameter for sXXX recommendation

## [1.0.0](http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/js/rb-warren-jsonp-1.0.0.jq.js) (2015-12-21)

### Features
* owlCarousel recommendation display library