# Introduction
HTML, CSS, JavaScript files for warren front-end

# Folder and Files Structures

* http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/{siteName}/
 * {siteName}-{channel}.html : HTML 코드 데모 페이지
 * {siteName}-{channel}.css : 사이트별 개별 적용 CSS
 * {siteName}-{channel}.js : 사이트별 추천 API 호출 파라미터 설정 JS
 * channel: pc / m 