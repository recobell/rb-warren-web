/*!
 * RecoBell Warren View Controller Library
 * http://www.recobell.com/
 * Copyright (c) 2015 RecoBell, all rights reserved.
 */

 function loadScript2(url, callback){
    var script = document.createElement('script');
    script.type = 'text/javascript';

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == 'loaded' ||
                    script.readyState == 'complete'){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            callback();
        };
    }
    script.src = url;
	head = document.getElementsByTagName ('head')[0] || document.documentElement;
	head.insertBefore(script, head.firstChild);
}
 
function numberWithCommas(n){var r=n.toString().split(".");return r[0]=r[0].replace(/\B(?=(\d{3})+(?!\d))/g,","),r.join(".")}
if (_rb_jsonp["ab_test"] != true || (_rb_jsonp["rb_pcId"] != "" && (_rb_jsonp["rb_pcId"].substring(_rb_jsonp["rb_pcId"].length - 1) * 1) % 2 == 0)) {
	loadScript2('http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/js/jquery-1.11.1.min.jq.js',function() {
		loadScript2('http://rb-warren-web-apne1.s3-website-ap-northeast-1.amazonaws.com/js/owl.carousel.jq.js',function() {
			jq('<style type="text/css">\n' + "/*  * Core Owl Carousel CSS File *v1.3.3 *//* clearfix */.owl-carousel .owl-wrapper:after {content: \".\";display: block;clear: both;visibility: hidden;line-height: 0;height: 0;}/* display none until init */.owl-carousel{display: none;position: relative;width: 100%;-ms-touch-action: pan-y;}.owl-carousel .owl-wrapper{display: none;position: relative;-webkit-transform: translate3d(0px, 0px, 0px);}.owl-carousel .owl-wrapper-outer{overflow: hidden;position: relative;width: 100%;}.owl-carousel .owl-wrapper-outer.autoHeight{-webkit-transition: height 500ms ease-in-out;-moz-transition: height 500ms ease-in-out;-ms-transition: height 500ms ease-in-out;-o-transition: height 500ms ease-in-out;transition: height 500ms ease-in-out;}.owl-carousel .owl-item{float: left;}.owl-controls .owl-page,.owl-controls .owl-buttons div{cursor: pointer;}.owl-controls {-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;-webkit-tap-highlight-color: rgba(0, 0, 0, 0);}/* mouse grab icon */.grabbing { /*    cursor:url(grabbing.png) 8 8, move;    */}/* fix */.owl-carousel  .owl-wrapper,.owl-carousel  .owl-item{-webkit-backface-visibility: hidden;-moz-backface-visibility:    hidden;-ms-backface-visibility:     hidden;  -webkit-transform: translate3d(0,0,0);  -moz-transform: translate3d(0,0,0);  -ms-transform: translate3d(0,0,0);}" + '</style>').appendTo("head");
			jq('<style type="text/css">\n' + "/** Owl Carousel Owl Demo Theme *v1.3.3*/.owl-theme .owl-controls{margin-top: 10px;text-align: center;}/* Styling Next and Prev buttons */.owl-theme .owl-controls .owl-buttons div{color: #FFF;display: inline-block;zoom: 1;*display: inline;/*IE7 life-saver */margin: 5px;padding: 3px 10px;font-size: 12px;-webkit-border-radius: 30px;-moz-border-radius: 30px;border-radius: 30px;background: #869791;filter: Alpha(Opacity\u003d50);/*IE7 fix*/opacity: 0.5;}/* Clickable class fix problem with hover on touch devices *//* Use it for non-touch hover action */.owl-theme .owl-controls.clickable .owl-buttons div:hover{filter: Alpha(Opacity\u003d100);/*IE7 fix*/opacity: 1;text-decoration: none;}/* Styling Pagination*/.owl-theme .owl-controls .owl-page{display: inline-block;zoom: 1;*display: inline;/*IE7 life-saver */}.owl-theme .owl-controls .owl-page span{display: block;width: 12px;height: 12px;margin: 5px 7px;filter: Alpha(Opacity\u003d50);/*IE7 fix*/opacity: 0.5;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;background: #869791;}.owl-theme .owl-controls .owl-page.active span,.owl-theme .owl-controls.clickable .owl-page:hover span{filter: Alpha(Opacity\u003d100);/*IE7 fix*/opacity: 1;}/* If PaginationNumbers is true */.owl-theme .owl-controls .owl-page span.owl-numbers{height: auto;width: auto;color: #FFF;padding: 2px 10px;font-size: 12px;-webkit-border-radius: 30px;-moz-border-radius: 30px;border-radius: 30px;}/* preloading images */.owl-item.loading{min-height: 150px;/*background: url(AjaxLoader.gif) no-repeat center center*/}" + '</style>').appendTo("head");
			jq('<style type="text/css">\n' + "/*  *  Owl Carousel CSS3 Transitions  *  v1.3.2 */.owl-origin {-webkit-perspective: 1200px;-webkit-perspective-origin-x : 50%;-webkit-perspective-origin-y : 50%;-moz-perspective : 1200px;-moz-perspective-origin-x : 50%;-moz-perspective-origin-y : 50%;perspective : 1200px;}/* fade */.owl-fade-out {  z-index: 10;  -webkit-animation: fadeOut .7s both ease;  -moz-animation: fadeOut .7s both ease;  animation: fadeOut .7s both ease;}.owl-fade-in {  -webkit-animation: fadeIn .7s both ease;  -moz-animation: fadeIn .7s both ease;  animation: fadeIn .7s both ease;}/* backSlide */.owl-backSlide-out {  -webkit-animation: backSlideOut 1s both ease;  -moz-animation: backSlideOut 1s both ease;  animation: backSlideOut 1s both ease;}.owl-backSlide-in {  -webkit-animation: backSlideIn 1s both ease;  -moz-animation: backSlideIn 1s both ease;  animation: backSlideIn 1s both ease;}/* goDown */.owl-goDown-out {  -webkit-animation: scaleToFade .7s ease both;  -moz-animation: scaleToFade .7s ease both;  animation: scaleToFade .7s ease both;}.owl-goDown-in {  -webkit-animation: goDown .6s ease both;  -moz-animation: goDown .6s ease both;  animation: goDown .6s ease both;}/* scaleUp */.owl-fadeUp-in {  -webkit-animation: scaleUpFrom .5s ease both;  -moz-animation: scaleUpFrom .5s ease both;  animation: scaleUpFrom .5s ease both;}.owl-fadeUp-out {  -webkit-animation: scaleUpTo .5s ease both;  -moz-animation: scaleUpTo .5s ease both;  animation: scaleUpTo .5s ease both;}/* Keyframes *//*empty*/@-webkit-keyframes empty {  0% {opacity: 1}}@-moz-keyframes empty {  0% {opacity: 1}}@keyframes empty {  0% {opacity: 1}}@-webkit-keyframes fadeIn {  0% { opacity:0; }  100% { opacity:1; }}@-moz-keyframes fadeIn {  0% { opacity:0; }  100% { opacity:1; }}@keyframes fadeIn {  0% { opacity:0; }  100% { opacity:1; }}@-webkit-keyframes fadeOut {  0% { opacity:1; }  100% { opacity:0; }}@-moz-keyframes fadeOut {  0% { opacity:1; }  100% { opacity:0; }}@keyframes fadeOut {  0% { opacity:1; }  100% { opacity:0; }}@-webkit-keyframes backSlideOut {  25% { opacity: .5; -webkit-transform: translateZ(-500px); }  75% { opacity: .5; -webkit-transform: translateZ(-500px) translateX(-200%); }  100% { opacity: .5; -webkit-transform: translateZ(-500px) translateX(-200%); }}@-moz-keyframes backSlideOut {  25% { opacity: .5; -moz-transform: translateZ(-500px); }  75% { opacity: .5; -moz-transform: translateZ(-500px) translateX(-200%); }  100% { opacity: .5; -moz-transform: translateZ(-500px) translateX(-200%); }}@keyframes backSlideOut {  25% { opacity: .5; transform: translateZ(-500px); }  75% { opacity: .5; transform: translateZ(-500px) translateX(-200%); }  100% { opacity: .5; transform: translateZ(-500px) translateX(-200%); }}@-webkit-keyframes backSlideIn {  0%, 25% { opacity: .5; -webkit-transform: translateZ(-500px) translateX(200%); }  75% { opacity: .5; -webkit-transform: translateZ(-500px); }  100% { opacity: 1; -webkit-transform: translateZ(0) translateX(0); }}@-moz-keyframes backSlideIn {  0%, 25% { opacity: .5; -moz-transform: translateZ(-500px) translateX(200%); }  75% { opacity: .5; -moz-transform: translateZ(-500px); }  100% { opacity: 1; -moz-transform: translateZ(0) translateX(0); }}@keyframes backSlideIn {  0%, 25% { opacity: .5; transform: translateZ(-500px) translateX(200%); }  75% { opacity: .5; transform: translateZ(-500px); }  100% { opacity: 1; transform: translateZ(0) translateX(0); }}@-webkit-keyframes scaleToFade {  to { opacity: 0; -webkit-transform: scale(.8); }}@-moz-keyframes scaleToFade {  to { opacity: 0; -moz-transform: scale(.8); }}@keyframes scaleToFade {  to { opacity: 0; transform: scale(.8); }}@-webkit-keyframes goDown {  from { -webkit-transform: translateY(-100%); }}@-moz-keyframes goDown {  from { -moz-transform: translateY(-100%); }}@keyframes goDown {  from { transform: translateY(-100%); }}@-webkit-keyframes scaleUpFrom {  from { opacity: 0; -webkit-transform: scale(1.5); }}@-moz-keyframes scaleUpFrom {  from { opacity: 0; -moz-transform: scale(1.5); }}@keyframes scaleUpFrom {  from { opacity: 0; transform: scale(1.5); }}@-webkit-keyframes scaleUpTo {  to { opacity: 0; -webkit-transform: scale(1.5); }}@-moz-keyframes scaleUpTo {  to { opacity: 0; -moz-transform: scale(1.5); }}@keyframes scaleUpTo {  to { opacity: 0; transform: scale(1.5); }}" + '</style>').appendTo("head");
			jq.ajax({
			    url: "http://rb-rec-api-apne1.recobell.io/rec/" + _rb_jsonp["recCode"] + "?cuid=" + _rb_jsonp["cuid"] + "&st=" + _rb_jsonp["st"] + "&format=jsonp&size=" + (_rb_jsonp["size"]*1 + 10) + "&iids=" + _rb_jsonp["iids"],
			    jsonp: "callback",
			    dataType: "jsonp",
				timeout: 1000,
				error: function(x, t, m) {
					if(t==="timeout") {
						jq(_rb_jsonp["target"]).hide();
					}
				},
			    success: function( response ) {
			    	count = 0;
			    	if (_rb_jsonp["isProductsShown"] == true) {
				        jq(response.products).each(function(i, val) {
				        	try {
					        	_rb_jsonp_item_html = _rb_jsonp["html"] || "";
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{originalPrice}/g, val.originalPrice);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{originalPriceComma}/g, numberWithCommas(val.originalPrice));
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemId}/g, val.itemId);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemUrl}/g, val.itemUrl);

					        	var imgUrl = val.product.itemImage;
					        	if (!imgUrl.includes(_rb_jsonp["baseUrl"])) imgUrl = _rb_jsonp["baseUrl"] + imgUrl;

					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemImage}/g, imgUrl);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemName}/g, val.itemName);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{salePrice}/g, val.salePrice);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{description}/g, val.description);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{discountRate}/g, Math.floor(100 - val.salePrice / val.originalPrice * 100));
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{salePriceComma}/g, numberWithCommas(val.salePrice));
					        	if (val.extraImage) _rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{extraImage}/g, val.extraImage);
					        	else _rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{extraImage}/g, '');
					            jq(_rb_jsonp["target"] + "-content").append(_rb_jsonp_item_html);
					            count++;
				            } catch (err) {
				            	console.log(err);
				            }
				        });
			    	}
			        jq(response.results).each(function(i, val) {
			        	if (count < Number(_rb_jsonp["size"])) {
				        	try {
					        	_rb_jsonp_item_html = _rb_jsonp["html"] || "";
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{originalPrice}/g, val.product.originalPrice);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{originalPriceComma}/g, numberWithCommas(val.product.originalPrice));
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemId}/g, val.itemId);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemUrl}/g, val.product.itemUrl);
					        	
					        	var imgUrl = val.product.itemImage;
					        	if (!imgUrl.includes(_rb_jsonp["baseUrl"])) imgUrl = _rb_jsonp["baseUrl"] + imgUrl;

					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemImage}/g, imgUrl);
					        	
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{itemName}/g, val.product.itemName);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{salePrice}/g, val.product.salePrice);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{description}/g, val.product.description);
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{discountRate}/g, Math.floor(100 - val.product.salePrice / val.product.originalPrice * 100));
					        	_rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{salePriceComma}/g, numberWithCommas(val.product.salePrice));
					        	if (val.product.extraImage) _rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{extraImage}/g, val.product.extraImage);
						        else _rb_jsonp_item_html = _rb_jsonp_item_html.replace(/{extraImage}/g, '');
					            jq(_rb_jsonp["target"] + "-content").append(_rb_jsonp_item_html);
					        } catch (err) {
					        	console.log(err);
					        } finally {
					            jq(_rb_jsonp["target"]).show();
					            count++;
					        }
				    	}
			        });
					jq(_rb_jsonp["target"] + "-content").owlCarousel(_rb_jsonp["options"]);
			    }
			});
		});
	});
}